# Introduction
The image_annotate tool allows to manualy annonate a batch of images.

# Configure
## Image window
```
image_window{
  size_x = 1024  #-1 for no limit
  size_y = 1024  #-1 for no limit
}
```
You can define `size_x` and `size_y` to limit window scale so large image are not displayed too big.
The default value is `-1` which is a special value that means 'no resize'.
Positive values are the maximum number of pixel the window can use.




## Input
```
input{                 
  folder = in/
  folder = in2/
  regex  = .*\.png
  regex  = .*\.jpg
  regex  = .*\.bmp
  regex  = .*\.webp
}
```

* You can write as many input blocks as you want
* You can add multiple folder in the same input block
* each folder will be scanned, and all files matching regex will be used as input images.
* NOTE: there is no deduplication of input file names, so don't use redundant regex, don't add the same folder twice


## Output
```
output{
  db_path = out/annotations.sqlite
}
```

* The results will be written to the defined sqlite database. 
* NOTE : if the database exists, its content is read in order to show (when possible) existing annotation
* Data is saved automatically on each change.


## Annotations
The runtime behaviour is controlled by linking triggers (key press, joystick button push, ...) and actions.
To add an annotation, we define a specific action (called param) that will display the annotation on the screen and save it into the output database.
Then we link this action to triggers (here pushing the button A on the joystick, or pushing the key A).

To find what should be written in the trigger, the image_annotate graphical window display on its last line the last event that was trigger.

```
param{
  name = A_OK      # (1) 
  caption = (A) OK # (2) 
  sqlite_name = ok # (3)
  type = check
}
link{
  trigger = KEY:20    #A key (5)
  trigger = JOY:0:b0  #A button on joystick 0 (5)
  action  = A_OK  (4)
}
```

* (1) The name of the action, must be unique. This name is used in the link at (4). It must start with a letter, and be composed of alphanumeric character or underscores.
* (2) How this action should be displayed in the graphical interface. If not defined, name is used.
* (3) The name of the column in the output database. If not defined, name is used. It must start with a letter, and be composed of alphanumeric character or underscores.
* (4) Which action should be executed, here the one named `A_OK` defined in the previous param block
* (5) If you don't know what to write in trigger, the last triggered trigger is displayed in white on the last line of the graphical interface and written in the terminal. You can try it by running the program with a dummy configuration file, and press a key on the keyboard. See section Triggers.

## Other actions (previous, next, quit ...)
The same principle can be used to control the interface with the following actions.
|action name| description |
|---|---|
|quit|Quits the interface. Note that all changes are saved as soon as they are made, so you don't have to save before quitting|
|previous|Displays the previous image|
|next    |Displays the next image|
|first   |Displays the first image|
|last    |Displays the last image|


```
link{
  trigger = KEY:41  #Escape
  action  = quit
}

link{
  trigger = KEY:80    # key left
  trigger = KEY:82    # key up
  trigger = KEY:75    # key pageup
  trigger = JOY:0:b4  # joystick0 left shoulder
  trigger = JOY:h0,0  # joystick0 D-pad left
  action  = previous
}

link{
  trigger = KEY:79    #key right
  trigger = KEY:81    #key down
  trigger = KEY:78    #key pagedown
  trigger = JOY:0:b5  #joystick0 right shoulder
  trigger = JOY:h0,2  #joystick0 D-pad right
  action  = next
}

link{
  trigger = KEY:74  #home
  action  = first
}

link{
  trigger = KEY:77  #end
  action  = last
}
```

## Triggers
If you don't know what to write in trigger, the last triggered trigger is displayed in white on the last line of the graphical interface and written in the terminal. You can try it by running the program with a dummy configuration file, and press a key on the keyboard. Currently the code uses the following trigger, where xxx and yyy are numbers.


|trigger|description|
|---|---|
|`KEY:xxx`|Keypress, `xxx` is the key scancode.|
|`JOY:xxx:b:yyy`|Joystick number `xxx`, press of button numbered `yyy`.|
|`JOY:xxx:h:yyy`|Joystick number `xxx`, press of hat numbered `yyy`.|
|`JOY:xxx:k:yyy`|Joystick number `xxx`, press of ball numbered `yyy`.|
|`MOU:xxx:c:yyy`|Click with mouse number `xxx` on button `yyy`.|
|`WHE:xxx:x0`|Left with wheel mouse of mouse number `xxx`.|
|`WHE:xxx:x1`|Right with wheel mouse of mouse number `xxx`.|
|`WHE:xxx:y0`|Down with wheel mouse of mouse number `xxx`.|
|`WHE:xxx:y1`|Up with wheel mouse of mouse number `xxx`.|

Joystick and mouse numbers starts at 0.


###Scancodes
[Scancodes](https://wiki.libsdl.org/SDL_Keycode) identifies the position of a particular key on a keyboard. It doesn't have to be confused with what's actually written on the key (i.e., the mapped character). For example key 'A' on an AZERTY keyboard and key Q on a QWERTY one are at the same position, and therefore have the same scancode (here 20). The configuration file will therefore use the same position for fingers regardless of what's actually written on the keyboard. Like events, the last event is always written to the terminal, and displayed to the last line (in white) of the graphical interface, so you can use it to find which key has which number. Here is an example, on an AZERTY keyboard.

|key|AZERTY|
|---|---|
|A|20|
|B|5|
|C|6|
|D|7|
|E|8|
|F|9|
|G|10|
|H|11|
|I|12|
|J|13|
|K|14|
|L|15|
|M|51|
|N|17|
|O|18|
|P|19|
|Q|7|
|R|21|
|S|22|
|T|23|
|U|24|
|V|25|
|W|29|
|X|37|
|Y|28|
|Z|26|
|up|82|
|down|81|
|left|80|
|right|79|
|enter|40|
|space|44|


# License

## image_annotate
 * Authors : Pierre Blavy, INRAE
 * License : [GPL-3.0+](https://www.gnu.org/licenses/gpl-3.0.txt)

## Used libraries
* SDL2, published under the [zlib license](https://www.libsdl.org/license.php), sources are [here](https://github.com/libsdl-org/SDL).
* SDL_gfx, published under the [zlib license](https://www.libsdl.org/license.php), sources are [here](https://github.com/davidsiaw/SDL2_gfx) and in `src/lib/SDL_gfx/`.
* SDL_image, published under the [zlib license](https://www.libsdl.org/license.php), sources are [here](https://github.com/libsdl-org/SDL_image).
* SQLITE3, published in the [public domain](https://www.sqlite.org/copyright.html), sources are in `src/lib/sqlite3/`.



