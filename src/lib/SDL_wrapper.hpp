#ifndef SDL_WRAPPER_PIERRE_HPP_
#define SDL_WRAPPER_PIERRE_HPP_

//WRAPS SDL and TTF stuff to use RAII and C++ structures

#include "SDL_handles.hpp"
#include <filesystem>


//=== Generic ===
struct Coord{
  int x=0;
  int y=0;
};




//=== SDL ===

[[nodiscard]] Make_raii_t<void> CPP_SDL_Init(Uint32 f);


unique_handle<SDL_Window> CPP_SDL_CreateWindow(const char *title       , int x, int y, int w, int h, Uint32 flags);
unique_handle<SDL_Window> CPP_SDL_CreateWindow(const std::string &title, int x, int y, int w, int h, Uint32 flags);
unique_handle<SDL_Renderer> CPP_SDL_CreateRenderer(SDL_Window * window, int index=-1, Uint32 flags=SDL_RENDERER_ACCELERATED);
unique_handle<SDL_Joystick> CPP_SDL_JoystickOpen(int index);

unique_handle<SDL_Texture>  CPP_SDL_CreateTextureFromSurface(SDL_Renderer* rend , SDL_Surface* surface);

//=== SDL_image ===

unique_handle<SDL_Surface> CPP_IMG_Load(const char *file);
unique_handle<SDL_Surface> CPP_IMG_Load(const std::string &s);
unique_handle<SDL_Surface> CPP_IMG_Load(const std::filesystem::path &p);



//=== TTF ===

[[nodiscard]] Make_raii_t<void> CPP_TTF_Init();

Coord CPP_TTF_SizeText(TTF_Font * text_font, const std::string &text );

unique_handle<TTF_Font> CPP_TTF_OpenFont(const std::string &font_path, int size);

unique_handle<SDL_Surface> CPP_TTF_RenderText_Solid(TTF_Font * text_font, const std::string &text, const SDL_Color &color); 
      





#endif
