#ifndef LIB_CONVERT_OPTIONAL_HPP_
#define LIB_CONVERT_OPTIONAL_HPP_


#include "convert.hpp"
#include <optional>


template<
  typename To_tt,
  typename From_tt,
  typename Context_tag_tt>
struct Convert_t<
  std::optional<To_tt>,
  From_tt,
  Context_tag_tt
>{
	typedef std::optional<To_tt> To_t;
	typedef From_tt From_t;
	typedef Context_tag_tt Context_tag_t;

	static To_t run(const From_t &f){
		std::optional<To_tt> r;
		try{r=convert<To_tt,Context_tag_tt>(f);}
		catch(...){}
		return r;
	}
};






/*
template<typename Numeric_t,  typename Context_tag_tt>
struct Convert_t<
  std::optional<Numeric_t>,
  std::string,
  Context_tag_tt,
  typename std::enable_if< std::is_integral_v<Integer_t>,  Context_tag_tt>::type
>{
	typedef std::optional<Integer_t> To_t;
	typedef std::string From_t;
	typedef Context_tag_tt Context_tag_t;

	static To_t run(const From_t &f){
		std::optional<Integer_t> r;
		try{r=convert<Integer_t,config::Config_tag>(f);}
		catch(...){}
		return r;
	}
};
*/







#endif /* LIB_CONVERT_OPTIONAL_HPP_ */
