#ifndef SDL_HALDLES_PIERRE_HPP_
#define SDL_HALDLES_PIERRE_HPP_

#include "make_handle.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>




#define MAKE_HANDLE_T_MACRO(type,deleter)\
  template<>\
  struct Make_handle_t<type>{ \
    Make_handle_t()=delete;\
    static unique_handle<type> run(type* w){\
      return unique_handle<type>(w,[](type*d){deleter(d);} );\
    }\
  };\
  
//--end macro



MAKE_HANDLE_T_MACRO(SDL_Window   , SDL_DestroyWindow   )
MAKE_HANDLE_T_MACRO(SDL_Texture  , SDL_DestroyTexture  )
MAKE_HANDLE_T_MACRO(SDL_Renderer , SDL_DestroyRenderer )
MAKE_HANDLE_T_MACRO(SDL_Surface  , SDL_FreeSurface     )
MAKE_HANDLE_T_MACRO(SDL_Joystick , [](SDL_Joystick* joy){if (SDL_JoystickGetAttached(joy)) {SDL_JoystickClose(joy);} } )


    

MAKE_HANDLE_T_MACRO(TTF_Font     , TTF_CloseFont ) 
  //https://stackoverflow.com/questions/22886500/how-to-render-text-in-sdl2
  //https://wiki.libsdl.org/SDL_ttf/TTF_CloseFont


#undef MAKE_HANDLE_T_MACRO
#endif
