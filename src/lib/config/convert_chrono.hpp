#ifndef LIB_CONFIG_CONVERT_CHRONO_HPP_
#define LIB_CONFIG_CONVERT_CHRONO_HPP_


#include "convert/convert.hpp"

#include <string>
#include <chrono>

namespace config{
  struct Config_tag;
}



template<typename Rep, typename Period, typename ...A>
struct Convert_t<std::chrono::duration<Rep, Period,A...>,std::string,config::Config_tag>{
	typedef std::chrono::duration<Rep, Period,A...>  To_t;
	typedef std::string From_t;

	static To_t run(const From_t &s){
		typedef decltype(std::declval<To_t>().count() ) count_t;
		count_t ticks = Convert_t<count_t,From_t,config::Config_tag>::run(s);
		return To_t(ticks);
	}
};



#endif /* LIB_CONFIG_CONVERT_CHRONO_HPP_ */
