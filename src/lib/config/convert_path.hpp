#ifndef CONFIG_CONVERT_PATH_HPP_
#define CONFIG_CONVERT_PATH_HPP_

#include <filesystem>


#include <convert/convert.hpp>
#include <string>

namespace config{
  struct Config_tag;
}

template<>
struct Convert_t<
  std::filesystem::path, //To_t
  std::string,           //From_t
  config::Config_tag     //Context
>{
	typedef std::filesystem::path   To_t;
	typedef std::string             From_t;

	static To_t run(const From_t &f){
		return std::filesystem::path(f);
	}
};


#endif
