#ifndef ACTION_PIERRE_HPP_
#define ACTION_PIERRE_HPP_

#include <memory>
#include <functional>
#include "config/config.hpp"


//--- Action ---
struct Action_abstract{
  Action_abstract()=default;
  virtual ~Action_abstract(){}
  virtual void execute()=0;
  void operator()(){execute();}
};

typedef std::unique_ptr<Action_abstract> Action_ptr;


//--- construct from config block ---
struct Action_cstr_conf_abstract{
  Action_cstr_conf_abstract()=default;
  virtual ~Action_cstr_conf_abstract(){}
  virtual Action_ptr new_action(const config::Config &c)=0;

};

typedef std::unique_ptr<Action_cstr_conf_abstract> Action_cstr_conf_ptr;


//--- construct from string ---
struct Action_cstr_str_abstract{
  Action_cstr_str_abstract()=default;
  virtual ~Action_cstr_str_abstract(){}
  virtual Action_ptr new_action(const std::string &s)=0;

};

typedef std::unique_ptr<Action_cstr_str_abstract> Action_cstr_str;



//--- cout and cerr ---
struct Action_cout:Action_abstract{
  std::string message;
  Action_cout(const std::string &m):message(m){}
  Action_cout(const config::Config &c);
  void execute()override final;
};

struct Action_cerr:Action_abstract{
  std::string message;
  Action_cerr(const std::string &m):message(m){}
  Action_cerr(const config::Config &c);
  void execute()override final;
};

struct Action_cstr_cout_cerr : Action_cstr_conf_abstract, Action_cstr_str_abstract{
    Action_ptr new_action(const std::string &s)    override final;
    Action_ptr new_action(const config::Config &c) override final;
};


//--- function ---



#include <iostream>
struct Action_function:Action_abstract{
  Action_function(){}
  template< typename Fn >Action_function(Fn f):fn(f){std::cout << "action "<<!!fn<<std::endl; }
  std::function<void()> fn=nullptr;
  void execute()override final;
};





/*

//--- Action factory ---
//Simple chain of responsabilities
Action_ptr new_action(const std::string    &s);
Action_ptr new_action(const config::Config &conf);



*/






#endif

