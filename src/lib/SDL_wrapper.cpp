#include "SDL_wrapper.hpp"

#include <SDL2/SDL_image.h>

#include <filesystem>
#include <stdexcept>

//===========
//=== SDL ===
//===========


//--- SDL_Init  ---
Make_raii_t<void> CPP_SDL_Init(Uint32 f){
    if (SDL_Init(f) != 0) {
        throw std::runtime_error(std::string("ERROR : cannot initialize SDL, error=") +   SDL_GetError() );
    }
    return make_raii(SDL_Quit);
} 

//--- SDL_CreateWindow ---
unique_handle<SDL_Window> CPP_SDL_CreateWindow(
  const char *title,
  int x, int y, int w,
  int h, Uint32 flags
){
  auto p = SDL_CreateWindow(title,x,y,w,h,flags);
  if(p==nullptr){
   throw std::runtime_error(std::string("ERROR : SDL_CreateWindow, error=") +   SDL_GetError() );
  }
  return make_handle<SDL_Window> ( p);
}

unique_handle<SDL_Window> CPP_SDL_CreateWindow(
  const std::string &title,
  int x, int y, int w,
  int h, Uint32 flags
){
   return CPP_SDL_CreateWindow(title.c_str(),x,y,w,h,flags);
}


//--- SDL_JoystickOpen ---
unique_handle<SDL_Joystick> CPP_SDL_JoystickOpen(int index){

  auto p = SDL_JoystickOpen(index);
  if(p==nullptr){
   throw std::runtime_error(std::string("ERROR : SDL_JoystickOpen, error=") +   SDL_GetError() );
  }
  return make_handle<SDL_Joystick> ( p);
}



//--- SDL_CreateRenderer ---
unique_handle<SDL_Renderer> CPP_SDL_CreateRenderer(SDL_Window * window, int index, Uint32 flags){
  auto p = SDL_CreateRenderer( window , index , flags)  ;
  if(p==nullptr){
   throw std::runtime_error(std::string("ERROR : SDL_CreateRenderer, error=") +   SDL_GetError() );
  }
  return make_handle( p );
}


unique_handle<SDL_Texture>  CPP_SDL_CreateTextureFromSurface(SDL_Renderer* rend , SDL_Surface* surface){
  auto p = SDL_CreateTextureFromSurface(rend, surface)  ;
  if(p==nullptr){
   throw std::runtime_error(std::string("ERROR : SDL_CreateTextureFromSurface, error=") +   SDL_GetError() );
  }
  return make_handle( p );
}





//=================
//=== SDL_image ===
//=================


unique_handle<SDL_Surface> CPP_IMG_Load(const char *file){
  auto p = IMG_Load(file)  ;
  if(p==nullptr){
   throw std::runtime_error(std::string("ERROR : IMG_Load, path="+std::string(file)+", error=") +   SDL_GetError() );
  }
  return make_handle( p );
}
unique_handle<SDL_Surface> CPP_IMG_Load(const std::string &s){
  return CPP_IMG_Load(s.c_str() );
}

unique_handle<SDL_Surface> CPP_IMG_Load(const std::filesystem::path &p){
  return CPP_IMG_Load(p.string().c_str() );
}
   



//===========
//=== TTF ===
//===========

//--- TTF_Init ---
Make_raii_t<void> CPP_TTF_Init(){
    if ( TTF_Init() != 0 ){
      throw std::runtime_error(std::string("ERROR : failed to initialize TTF, error=") +SDL_GetError() );
    }
    return make_raii(TTF_Quit);
}
    
   

//--- TTF_SizeText ---
Coord CPP_TTF_SizeText(TTF_Font * text_font, const std::string &text ){
  Coord coord;
  if (   TTF_SizeText(text_font , text.c_str() , &coord.x , &coord.y ) != 0  ){
    throw std::runtime_error(std::string("ERROR : cannot guess text size, error=") + SDL_GetError() );
  }
  return coord;
}



//--- TTF_OpenFont ---
unique_handle<TTF_Font> CPP_TTF_OpenFont(const std::string &font_path, int size){
    auto text_font =  make_handle<TTF_Font> (  TTF_OpenFont( font_path.c_str() , size )  );
    if(text_font==nullptr){
      throw  std::runtime_error(std::string("ERROR : cannot open font, path=") +font_path+", error=" + SDL_GetError() );
    }
    return text_font;
}

//--- TTF_RenderText_Solid ---
unique_handle<SDL_Surface> CPP_TTF_RenderText_Solid(TTF_Font * text_font, const std::string &text, const SDL_Color &color){
    if(text==""){
      return CPP_TTF_RenderText_Solid(text_font," ",color);
    }
    
    
    auto p =  TTF_RenderText_Solid(text_font,text.c_str(), color);
    if(p==nullptr){
      throw std::runtime_error(std::string("ERROR : TTF_RenderText_Solid, error=") +   SDL_GetError() );
    }
    return make_handle( p );
}








    
