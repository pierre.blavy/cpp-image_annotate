#ifndef MAKE_HANDLE_PIERRE_HPP
#define MAKE_HANDLE_PIERRE_HPP

#include <memory>
#include <functional>

template<typename T> 
using unique_handle = std::unique_ptr<T, std::function<void(T*)>  >;

template<typename T> 
class Make_handle_t;  //Must be specialized, see example

template<typename T> 
unique_handle<T> make_handle(T*x){return Make_handle_t<T>::run(x);}

template<typename T, typename Fn> 
unique_handle<T> make_handle(T*x, Fn deleter){return unique_handle<T>(x,deleter) ;}




template<typename Context=void> //Can be specialized
struct Make_raii_t{
	Make_raii_t()=delete;
	
	template<typename Fn>
	explicit Make_raii_t(Fn f):on_delete(f){}
	
	~Make_raii_t(){on_delete();}
	std::function<void()> on_delete;
};


template<typename Context=void, typename Fn> 
auto make_raii(Fn f){return Make_raii_t<Context>(f); }



//--- EXAMPLE : specialize make_handle to use SDL_DestroyWindow as default deleter of SDL_Window ---
/*
template<> 
struct Make_handle_t<SDL_Window>{
  Make_handle_t()=delete;
  static unique_handle<SDL_Window> run(SDL_Window*w){
    return unique_handle<SDL_Window>(w,[](SDL_Window*d){SDL_DestroyWindow(d);} );
  }
};


auto win = make_handle<SDL_Window>(SDL_CreateWindow(...) );
*/








#endif
