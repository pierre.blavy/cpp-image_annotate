#include "Actions.hpp"

#include <regex>
#include <iostream>

//--- str_split2 ---
std::pair<std::string,std::string>  str_split2(const std::string &s){
  static const std::regex re1 ("^([^\\s]+)[\\s]+(.*?)[\\s]*$");  //split and trim
  static const std::regex re2 ("^[\\s]*(.*?)[\\s]*$");  //trim
  
  //try the split and trim
  std::smatch sm;
  std::regex_search(s, sm, re1);
  if(sm.size()==3){return {sm[1],sm[2]}; }
  
  //trim
  //recycle sm
  std::regex_search(s, sm, re2);
  if(sm.size()==2){return {sm[1],""}; }
  
  return {s,""};
  
}








//--- cout and cerr ---

Action_cout::Action_cout(const config::Config &c):
   message(c.values.get_unique("message"))
{} 

void Action_cout::execute(){
  std::cout << message;
}


Action_cerr::Action_cerr(const config::Config &c):
   message(c.values.get_unique("message"))
{}

void Action_cerr::execute(){
  std::cerr << message;
}


Action_ptr Action_cstr_cout_cerr::new_action(const std::string &s){
   auto command = str_split2(s);
   if(command.first == "cout"){return std::make_unique<Action_cout>(command.second); }
   if(command.first == "cerr"){return std::make_unique<Action_cerr>(command.second); } 
   return nullptr;

}

Action_ptr Action_cstr_cout_cerr::new_action(const config::Config &c){
  if(c.values.get_unique("type") == "cout"){return std::make_unique<Action_cout>(c); }
  if(c.values.get_unique("type") == "cerr"){return std::make_unique<Action_cerr>(c); }
  return nullptr;
}


//--- function ---



void Action_function::execute(){ 
    if(fn){fn(); return;}
    throw std::runtime_error("cannot execute null function");
}



//--- FACTORIES ---
/*

Action_ptr new_action(const std::string &s){
    Action_ptr r=nullptr;
    r=Action_cstr_cout_cerr().new_action(s); if(r!=nullptr){return r;}
    
    throw std::runtime_error("cannot construct action from string, string="+s);
    
}

Action_ptr new_action(const config::Config &c){
    Action_ptr r=nullptr;
    r=Action_cstr_cout_cerr().new_action(c); if(r!=nullptr){return r;}
    
    throw std::runtime_error("cannot construct action from config, config="+c.debug_str() );
    
}

*/ 
