#include "is_name.hpp"
#include <regex>

//doc : optimlize regex https://www.youtube.com/watch?v=7hfSyxNxFfo
// put smatch outside the loop
// regex consturction takes time
bool str::is_name(const std::string &s){
  static const std::regex re("[A-Za-z][A-Za-z0-9_]*",std::regex::optimize);
  std::smatch sm;
  return (regex_search(s, sm, re) == true) ;
}




