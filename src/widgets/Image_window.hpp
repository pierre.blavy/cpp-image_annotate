#ifndef IMAGE_WINDOW_PIERRE_HPP_
#define IMAGE_WINDOW_PIERRE_HPP_

#include "SDL_wrapper.hpp"

#include <vector>
#include <filesystem>
#include <limits>



struct Image_window{
  std::vector<std::filesystem::path> image_path_vect ;
  size_t last_display = std::numeric_limits<size_t>::max();
    
  unique_handle<SDL_Window>   window;
  unique_handle<SDL_Renderer> renderer; 
  unique_handle<SDL_Surface>  image;  
  
  size_t current_index = 0;
  void next()    {if(current_index+1<size() ){++current_index; update(); } }
  void previous(){if(current_index>0){--current_index; update(); } }
  void first(){current_index=0; update(); }
  void last() {if(size()==0){current_index=0;}else{current_index = size() -1 ;}  update();  }
  void increment(int i);
  
  std::string current_filename()const;
  
  
  void add_folder(const std::filesystem::path &p, const std::string &regex_str);
  
  Image_window();
  
  void add_path(const std::filesystem::path &p);
  size_t size()const ;
  

  
  //<0 = auto
  //>0 = max size
  int size_x = -1;
  int size_y = -1;
  
  
  void update (bool force=false);

};





#endif
