#include "Image_window.hpp"

#include "SDL_gfx/SDL2_rotozoom.h"

#include <regex>
#include <iostream>


Image_window::Image_window(){
      window = CPP_SDL_CreateWindow(
      "image",
      SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED,
      64,64,
      SDL_WINDOW_RESIZABLE
    );
    
     renderer  = CPP_SDL_CreateRenderer( window.get() );
}
  
  
  

void Image_window::add_path(const std::filesystem::path &p){
    image_path_vect.push_back(p);
}
  

size_t Image_window::size()const {
  return image_path_vect.size();
}
  
std::string Image_window::current_filename()const {
  if(current_index<size() ){
    return image_path_vect[current_index].filename().string();
  }else{
    return "UNKNOWN FILE";
  }
} 
   
   
void Image_window::update(bool force ){
    if( current_index >=size() ){return;}
    if(current_index == last_display and !force ) {return;}
    last_display=current_index;
    
    
    
    image=CPP_IMG_Load(image_path_vect[current_index] );
    

    
    //get texture size
    int    x=64,y=64;
    SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer.get() , image.get() );
    SDL_QueryTexture(texture, NULL,NULL,&x,&y);
    
    
    //if there is a window limit, use it :
    double zoom = 1;
    bool need_resize = false;
    if(size_x >0 and x > size_x){
      zoom = std::min(zoom, ( double(x) /size_x) );
      need_resize=true;
      x = size_x;
    }
    if(size_y >0 and y > size_y){
      zoom = std::min(zoom, ( double(y) /size_y) );
      need_resize=true;   
      y = size_y;
    }
    
    
    if(need_resize){
	    SDL_Surface * tmp = rotozoomSurface( image.get() , 0, zoom, 1);
	    SDL_DestroyTexture(texture);
	    texture = SDL_CreateTextureFromSurface(renderer.get() , tmp );
	    SDL_FreeSurface(tmp);
    }
    
    
    SDL_SetWindowSize(window.get() ,x ,y);
    SDL_SetWindowTitle(window.get() , ( 
      std::to_string(current_index+1 )  + "/"+
      std::to_string( size() ) + " "+ 
      current_filename()   
    ) .c_str()  );
    
    SDL_RenderCopy(renderer.get(), texture, NULL, NULL);
    
    
    SDL_RenderPresent(renderer.get());
    
    SDL_DestroyTexture(texture);
    

}



void Image_window::add_folder(const std::filesystem::path &p, const std::string &regex_str){
  std::regex re(regex_str);
  std::smatch sm;
  std::cout << "INPUT:"<<p <<" " << regex_str <<std::endl;
  
  
  for (const auto& dirEntry : std::filesystem::directory_iterator(p)){
    //skip if not a file
    if(! dirEntry.is_regular_file() ){continue;}
  
    //skip if doesn't match regex
    std::string s = dirEntry.path().string();
    std::regex_search(s , sm, re);
    if(sm.size()==0){continue;}
    
    std::cout <<"  "<< dirEntry << std::endl;
    add_path( std::filesystem::canonical(dirEntry.path() ) );
  }
  
}


void Image_window::increment(int i){
    if(i>0){
      current_index+=i;
      if(current_index>=size() ){
        if(size()!=0){current_index=size() - 1;}
        else{current_index = 0;}
      }
    }
    
    if(i<0){
      i=-i;
      if(i>current_index){current_index=0;}
      else{current_index-=i;}
    }
}

