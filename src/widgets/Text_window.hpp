#ifndef TEXT_WINDOW_PIERRE_HPP_
#define TEXT_WINDOW_PIERRE_HPP_

#include <string>
#include "SDL_wrapper.hpp"


//--- Text_properties ---

struct Text_item{
  Text_item()=default;
  Text_item(const std::string &t, const SDL_Color&c={255,255,255} ): text(t),color(c){}


  std::string text;
  
  int margin_left   = 4;
  int margin_right  = 4;
  int margin_top    = 4;
  int margin_bottom = 4;
  int font_size     = 24;
  

  
  SDL_Color color;
};



struct Text_window{
  Text_window(const std::string & font_path);
  std::vector<Text_item> text;
  
  void update();
  
  
  Coord get_coord()const;
  unique_handle<SDL_Window>   window;
  unique_handle<TTF_Font>     text_font;
  unique_handle<SDL_Renderer> renderer; 

  
};






    
    
    
#endif
