#include "Text_window.hpp"


//--- helpers ---
void add_text_size(Coord &add_here, const Text_item &it,   TTF_Font * text_font ){    
    //text size
    Coord c = CPP_TTF_SizeText( text_font , it.text );
    c.x   += it.margin_left + it.margin_right;
    c.y   += it.margin_top +  it.margin_bottom;
    
    if(c.x > add_here.x ){add_here.x = c.x;}
    add_here.y += c.y;
}


Text_window::Text_window(const std::string &font_path){
   window = CPP_SDL_CreateWindow(
      "status",
      SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED,
      64,64,
      SDL_WINDOW_RESIZABLE
    );
    
   renderer  = CPP_SDL_CreateRenderer( window.get() );
   text_font = CPP_TTF_OpenFont(font_path , 24);
}




void Text_window::update(){


  //window size
  {
    Coord text_size;
    for(const auto &p : text){
      add_text_size(text_size, p, text_font.get() );
    }
    SDL_SetWindowSize(window.get() ,text_size.x ,text_size.y);
  }
  
  //window clear
  SDL_SetRenderDrawColor(renderer.get() , 0, 0, 0, 255);
  SDL_RenderClear(renderer.get());
  
  
  //draw text
  int y_offset = 0;
  for(const auto &p : text){
      auto text_size = CPP_TTF_SizeText(text_font.get() , p.text );
      auto surface_message = CPP_TTF_RenderText_Solid(
        text_font.get(), 
        p.text, 
        p.color  
      ); 
      auto message = CPP_SDL_CreateTextureFromSurface(renderer.get() , surface_message.get() );
      
      
      SDL_Rect message_rect; //create a rect
      message_rect.x = p.margin_left ; 
      message_rect.y = y_offset + p.margin_top; 
      message_rect.w = text_size.x;
      message_rect.h = text_size.y;
      
      SDL_RenderCopy(renderer.get() , message.get() , NULL, &message_rect);
      
      //SDL_FreeSurface(surface_message);
      //SDL_DestroyTexture(message);
      
      y_offset+=p.margin_top+p.margin_bottom + text_size.y;
  }
  
  SDL_RenderPresent(renderer.get());
}


