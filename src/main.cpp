
//undefined reference to `WinMain@16'
// => add -DSDL_MAIN_HANDLED to compiler


#include "config/config.hpp"
#include "widgets/Text_window.hpp"
#include "widgets/Image_window.hpp"

#include "tdb/tdb_sqlite.hpp"

#include "str/is_name.hpp"

#include <deque>
#include <iostream>
#include <filesystem>
#include <stdexcept>
#include <unordered_map>

#include <Actions.hpp>



//std::unordered_map<std::string, Action_cstr_conf_ptr> action_cstr_map_conf;
//std::unordered_map<std::string, Action_cstr_str_ptr>  action_cstr_map_str;

//TODO register constructors





struct Image_param{

  
  Image_param()=default;
  explicit Image_param(const config::Config &c){configure(c);}
  
  
  std::string get_text()const{
    if(is_checked){return "X "+caption;}
    else          {return "  "+caption;}
  }
  
  const SDL_Color &get_color()const{
    return is_checked?color_checked:color_unchecked;
  }
  
  
  void configure(const config::Config &c){
      //load name
      c.values.load_unique(name,"name");
      c.values.load_unique(caption    ,"caption"    ,name);
      c.values.load_unique(sqlite_name,"sqlite_name",name);
      
      //name and sqlite_name must be valid names (i.e., start with a letter, then alpha numeric underscore)
      if(!str::is_name(name)        ){throw std::runtime_error("invalid name at" + c.debug_str() );  }
      if(!str::is_name(sqlite_name) ){throw std::runtime_error("invalid sqlite_name at" + c.debug_str() );  } 
      
  }
  
  void toggle(){is_checked = !is_checked; std::cout << "toggle " << name <<std::endl;}
  
  bool is_checked=false;
  std::string name;
  std::string caption; 
  std::string sqlite_name; 
    
  static constexpr SDL_Color color_checked   = {0,255,0};
  static constexpr SDL_Color color_unchecked = {255,0,0};
  
};



//--- TODO database ---
//TODO index params in a map, so we can find them by name

typedef tdb::Connection_t<tdb::Tag_sqlite> Database_t;

void db_load_params(Database_t &db, std::deque<Image_param> &write_here, const std::string &image_name ){
      
   for(Image_param &p : write_here){
     //try to get value from database
     try{
       auto query =  tdb::prepare_new< std::tuple<int> , std::tuple<std::string> >(db ,"select `" + p.sqlite_name +"` from image_annotation natural join images where image_name = ?");
       auto result = tdb::get_result_a(query, image_name);
       if( auto r = try_fetch(result) ){
         p.is_checked = std::get<0>( r.value() ) != 0 ;
       }else{
         p.is_checked = false;
       }
     }catch(std::exception &e){
       std::cerr << e.what()<<std::endl;
     }    
   }
}

void db_create_params(Database_t &db, const std::deque<Image_param> &params ){
   auto tr = tdb::transaction(db);
   for(const Image_param &p : params){
      try{
        tdb::execute(db, "alter table image_annotation add column `" + p.sqlite_name +"` integer default 0 not null;");
      }catch(std::exception &e){
        //std::cerr << e.what()<<std::endl;
      }
   }
   tr.commit();
}


void db_save_param(Database_t &db, const std::deque<Image_param> &params, const std::string &image_name ){
   auto tr = tdb::transaction(db);
   for(const Image_param &p : params){
      try{
        tdb::execute_a(db, "update image_annotation set`" + p.sqlite_name +"`=? where image_id in (select image_id from images where image_name=?)", p.is_checked,image_name );
        tdb::execute_a(db, "insert or ignore into image_annotation (image_id,`"+p.sqlite_name+"` ) values ( (select image_id from images where image_name=?) , ?)  ", image_name , p.is_checked );
      }catch(std::exception &e){
        std::cerr << e.what()<<std::endl;
      }
   }
   tr.commit();
}

void db_save_image(Database_t &db, const std::string &image_name, const std::string&path ){
    //transaction in caller
      try{
        tdb::execute_a(db, "insert or ignore into images (image_name,image_path) values ( ? , ?)  ", image_name , path );
        tdb::execute_a(db, "update images set image_path= ? where image_name = ? ",  path , image_name );
      }catch(std::exception &e){
        std::cerr << e.what()<<std::endl;
      }
}



     
      
std::string event_name(const SDL_Event &event){
  auto uts=[](Uint8 x){return std::to_string(x) ; };

  switch(event.type){
          case SDL_KEYDOWN:      {return std::string("KEY:") + uts(event.key.keysym.scancode);}
          case SDL_JOYBUTTONDOWN:{return std::string("JOY:") + uts(event.jbutton.which) + ":b" + uts(event.jbutton.button);}
          case SDL_JOYHATMOTION: {return std::string("JOY:") + uts(event.jhat.which)    + ":h" + uts(event.jhat.hat)      + "," + uts(event.jhat.value);}
          case SDL_JOYBALLMOTION:{return std::string("JOY:") + uts(event.jball.which)   + ":k" + uts(event.jball.ball)    + "," + uts(event.jball.xrel) + "x" + uts(event.jball.yrel);}
          case SDL_MOUSEBUTTONDOWN:{
            if(event.button.state == SDL_PRESSED){
              return std::string("MOU:") + uts(event.button.which)   + ":c" + uts(event.button.button);
            }
          }
          case SDL_MOUSEWHEEL:{
            if(event.wheel.x>0){return std::string("WHE:") + uts(event.wheel.which) +":x1";}
            if(event.wheel.x<0){return std::string("WHE:") + uts(event.wheel.which) +":x0";}
            if(event.wheel.y>0){return std::string("WHE:") + uts(event.wheel.which) +":y1";}
            if(event.wheel.y<0){return std::string("WHE:") + uts(event.wheel.which) +":y0";}
          }
          
  }
  
  return "";
}
      
      


int main(int argn, char* argv[] ){

    //--- general state ---
    bool quit = false; //main loop
    std::deque<Image_param> all_params;

    //TODO parse commandline
    if(argn<1){std::cerr << "ERROR : no working directory"; return 0;}
    std::string path_executable = std::filesystem::canonical( std::filesystem::path(argv[0]) ).remove_filename().string();
    

    //--- set path_config, read config ---
    std::string path_config;
    size_t start_index=0;
    if(argn==1){path_config= "config.txt";}
    if(argn>=2){path_config= argv[1];}
    if(argn==3){start_index= stoi( std::string(argv[2]) ); }
    if(argn>3 ){throw std::runtime_error("Invalid commandline, please use : image_annotate [path_to_config.txt] [start_index]"); }
    std::cout << "config path:"<< path_config<<std::endl;
    config::Config config;
    config.load(path_config);

    //--- open output database ---
    std::string db_path = config.blocks.get_unique("output").values.get_unique("db_path") ;
    Database_t db(db_path);
    tdb::read_file(db,"sql/database.sql");

 
    //--- init SDL, TTF, fonts ---
    auto sdl_raii  = CPP_SDL_Init(SDL_INIT_EVERYTHING);
    auto ttf_raii  = CPP_TTF_Init();
    auto text_font = CPP_TTF_OpenFont(path_executable+"/ubuntu-mono/UbuntuMono-R.ttf" , 24);

    //--- create widgets, load images ---
    Text_window status_window( path_executable+"/ubuntu-mono/UbuntuMono-R.ttf" );
    //for(const auto &p : all_params){status_window.text.emplace_back(p.get_text() , p.get_color()  );}
    
    Image_window image_window;
    for(const auto &block   : config.blocks.crange("input") ){ 
    for(const auto &path_v  : block.values.crange("folder") ){ 
    for(const auto &regex_v : block.values.crange("regex")  ){
      try{
        image_window.add_folder(path_v.to<std::string>() , regex_v.to<std::string>()   );
      }catch(std::exception &e){
        throw std::runtime_error("Cannot load folder"
        "\n  path="  + path_v .to<std::string>() +
        "\n  regex=" + regex_v.to<std::string>() +
        "\n  error=" + e.what() +
        "\n");
      }
    }}}
    image_window.increment(start_index);
    image_window.previous(); //first image is number 1
    
    //--- save image to database ---
    {
      auto tr = tdb::transaction(db);
      for(const std::filesystem::path &p : image_window.image_path_vect){
        db_save_image(db, p.filename().string(), std::filesystem::canonical(p ).string()   );
      }
      tr.commit();
    }
    
    
    
    //--- Joysticks ---
    //https://alexandre-laurent.developpez.com/tutoriels/sdl/joysticks/
    int  joystick_count = SDL_NumJoysticks();
    std::cout << joystick_count<< " joystick found : "<<joystick_count <<std::endl;
    std::vector< unique_handle<SDL_Joystick> > all_joysticks;
    all_joysticks.reserve(joystick_count);
    
    for(int i = 0; i < joystick_count; ++i){
    	all_joysticks.emplace_back( CPP_SDL_JoystickOpen(i) );
    	SDL_Joystick* joy = all_joysticks.back().get() ;
        std::cout << "Joystick "<<i<<"\n"
            << "Name:" << SDL_JoystickNameForIndex(i)<<"\n"
            << "Number of Axes:" << SDL_JoystickNumAxes(joy)<<"\n"
            << "Number of Buttons:" << SDL_JoystickNumButtons(joy)<<"\n"
            << "Number of Balls:" << SDL_JoystickNumBalls(joy)<<"\n"
            << std::endl;
    }
    
    if(joystick_count!=0){
      SDL_JoystickEventState(SDL_ENABLE); //enable joystick events as SDL_event
    }
     
     
     
    //--- generate actions ---
    std::unordered_map<std::string, Action_ptr> action_map;  //named actions
    action_map["quit"]     = std::make_unique<Action_function>([&quit](){quit=true;} );
    
    action_map["previous"] = std::make_unique<Action_function>([&](){image_window.previous(); db_load_params(db,all_params,image_window.current_filename() ); } );
    action_map["next"]     = std::make_unique<Action_function>([&](){image_window.next();     db_load_params(db,all_params,image_window.current_filename() ); } );
    action_map["first"]    = std::make_unique<Action_function>([&](){image_window.first();    db_load_params(db,all_params,image_window.current_filename() ); } );
    action_map["last"]     = std::make_unique<Action_function>([&](){image_window.last();     db_load_params(db,all_params,image_window.current_filename() ); } );    
    
    //--- load params and generate their actions ---
    for(const auto & block :config.blocks.crange("param") ){
      std::string name = block.values.get_unique("name");
      if(action_map.find(name)!=action_map.end() ){throw std::runtime_error("duplicated action name : "+name); }
      
      std::string type = block.values.get_unique("type");
      if(type=="check"){  
        all_params.emplace_back(block);
        Image_param* change_me = &all_params.back() ;
        status_window.text.push_back(change_me->get_text() );
        action_map[name] = std::make_unique<Action_function>([change_me,&db,&all_params,&image_window](){change_me->toggle();  db_save_param(db,all_params, image_window.current_filename() ); } );  
      }
      
    }
    
    
    //--- alter database to save params (i.e., create columns) ---
    db_create_params(db, all_params ); 
    
    
    
    
    //--- links ---
    std::unordered_map<std::string , std::vector<Action_abstract*>  > link_map;
    for(const auto & block :config.blocks.crange("link") ){
      for(const config::Value &trigger : block.values.crange("trigger") ){
      for(const config::Value &action  : block.values.crange("action") ){
        const std::string& action_name = action.to<std::string>();
        const auto f = action_map.find( action_name );
        if(f==action_map.end() ){
          throw std::runtime_error("missing action, action name=" + action_name+", at="+action.debug_str() );
        }
        Action_abstract* p = f->second.get() ;
        if(p==nullptr){throw std::runtime_error("null action");}//error here : code is wrong
        link_map[ trigger.to<std::string>() ].push_back(p);
      }}
    }
    
    //--- last_event ---
    status_window.text.emplace_back("",SDL_Color{255,255,255} );
    Text_item* last_event_it = &status_window.text.back();
    std::cout << "--- running main loop ---"<<std::endl;
    

    
    //image_window config
    image_window.size_x = config.blocks.get_unique("image_window").values.get_unique<int>("size_x",-1);
    image_window.size_y = config.blocks.get_unique("image_window").values.get_unique<int>("size_y",-1);


   //Main loop

   SDL_Event event;
   while( !quit ){

   
    //update screen
    for(size_t i = 0 ; i < all_params.size(); ++i ){
      Text_item & tt = status_window.text.at(i);
      tt.text  = all_params[i].get_text() ;
      tt.color = all_params[i].get_color() ;
      //std::cout << all_params[i].get_text()<< all_params[i].is_checked <<std::endl;
    }
    status_window.update();
    image_window.update(0);

    //Handle events on queue
    while( SDL_PollEvent( &event ) != 0 )
    {
	switch (event.type) {
          case SDL_QUIT:{std::cout << "QUIT"<<std::endl; quit = true; break;}
          case SDL_WINDOWEVENT:{
            switch (event.window.event) {
              case SDL_WINDOWEVENT_CLOSE:{std::cout << "QUIT"<<std::endl; quit = true; break;}
            }
            break;
          }//end SDL_WINDOWEVENT
          
          
        }//end switch
        
        std::string ev_str = event_name(event);
        if(ev_str!=""){
        
          //show last_event
          std::cout << ev_str << std::endl;
          last_event_it->text=ev_str;

          //execute links:
          const auto f = link_map.find(ev_str);
          if(f != link_map.end() ){
            std::cout << "execute n="<<f->second.size()<<std::endl;
            for(Action_abstract * action : f->second){action->execute() ; }
          }
          
       }
    }
    SDL_Delay(1000/60);
  }
         
    return 0;

}



