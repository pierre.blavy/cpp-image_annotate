#!/bin/bash

#CPP_FLAGS="-std=c++17 -O3  -m32 -march=i686  -static-libgcc -static-libstdc++"
#C_FLAGS="-std=c99 -O3 -m32 -march=i686 -static-libgcc"

OUT_FOLDER=image_annotate-linux
OUT_BIN=image_annotate

CPP_COMPILER="g++"
CPP_FLAGS="-std=c++17 -O3 "
C_FLAGS="-std=c99 -O3"


C_COMPILER="gcc"
C_FLAGS=""


rm -rf "$OUT_FOLDER"
rm -f "$OUT_FOLDER.tar.gz"

echo "$OUT_FOLDER";
mkdir "$OUT_FOLDER";


echo "--- SQLITE3 ---"
rm -f "$OUT_FOLDER/libsqlite3.o"
gcc $C_FLAGS -c -o "$OUT_FOLDER/libsqlite3.o" ./src/lib/sqlite3/sqlite3.c

echo "--- GFX ---"
#must be linked BEFORE SDL libs, as it depends on SDL
"$C_COMPILER" $C_FLAGS -c -o "$OUT_FOLDER/SDL2_rotozoom.o" src/lib/SDL_gfx/SDL2_rotozoom.c
"$C_COMPILER" $C_FLAGS -c -o "$OUT_FOLDER/SDL2_imageFilter.o" src/lib/SDL_gfx/SDL2_imageFilter.c
"$C_COMPILER" $C_FLAGS -c -o "$OUT_FOLDER/SDL2_gfxPrimitives.o" src/lib/SDL_gfx/SDL2_gfxPrimitives.c
"$C_COMPILER" $C_FLAGS -c -o "$OUT_FOLDER/SDL2_framerate.o" src/lib/SDL_gfx/SDL2_framerate.c





echo "--- CONFIG SDL2 ---"
#There is an utility that guesses the SDL flags called sdl2-config
#sdl2-config --libs --cflags will return
#  -lSDL2
#  -I/usr/include/SDL2 -D_REENTRANT
SDL2_CONFIG_FLAGS=`sdl2-config --cflags`   # -I/usr/include/SDL2 -D_REENTRANT
SDL2_CONFIG_LIBS=`sdl2-config --libs`      # -lSDL2

echo "  flags:" "$SDL2_CONFIG_FLAGS"
echo "  libs:" "$SDL2_CONFIG_LIBS"



echo "--- COMPILE_LINUX ---"
CPP_FILES=$(<compile-cpp.txt)

"$CPP_COMPILER"  -s -o "$OUT_FOLDER/$OUT_BIN" $CPP_FLAGS \
  -I src/lib \
  $SDL2_CONFIG_FLAGS \
  $CPP_FILES \
  "$OUT_FOLDER/libsqlite3.o" \
  "$OUT_FOLDER/SDL2_rotozoom.o" \
  "$OUT_FOLDER/SDL2_imageFilter.o" \
  "$OUT_FOLDER/SDL2_gfxPrimitives.o" \
  "$OUT_FOLDER/SDL2_framerate.o" \
  -ldl \
  $SDL2_CONFIG_LIBS \
  -lSDL2_image \
  -lSDL2_ttf \
;


rm -f "$OUT_FOLDER/libsqlite3.o"
rm -f "$OUT_FOLDER/SDL2_rotozoom.o"
rm -f "$OUT_FOLDER/SDL2_imageFilter.o"
rm -f "$OUT_FOLDER/SDL2_gfxPrimitives.o"
rm -f "$OUT_FOLDER/SDL2_framerate.o"

cp    "config.txt"  "$OUT_FOLDER"
cp -r "src/ubuntu-mono" "$OUT_FOLDER"
cp -r "sql/" "$OUT_FOLDER"




#  -I src/lib/sqlite3 \
#  "$OUT_FOLDER/libsqlite3.o" \
