create table if not exists images(
  image_id    integer primary key autoincrement,
  image_name  varchar unique not null,
  image_path  varchar not null
);



create table if not exists image_annotation(
  image_id    integer primary key ,
  foreign key (image_id) references images(image_id) on delete cascade
);

-- columns will be added for each param with alter table image_annotation add column xxxx integer default 0 not null;
--   -- if column exists, errors are ignored




