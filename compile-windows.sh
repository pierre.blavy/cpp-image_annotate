#!/bin/bash

#CPP_FLAGS="-std=c++17 -O3  -m32 -march=i686  -static-libgcc -static-libstdc++"
#C_FLAGS="-std=c99 -O3 -m32 -march=i686 -static-libgcc"




CPP_FLAGS="-std=c++17 -O3 "
C_FLAGS="-std=c99 -O3"


compile_windows () {
  BITS=$1
  PREFIX=$2

  OUT_FOLDER=image_annotate-win$BITS
  OUT_BIN=image_annotate.exe
  
  C_COMPILER="$PREFIX-gcc"
  CPP_COMPILER="$PREFIX-g++"

  
  echo c_compiler:"$C_COMPILER"
  echo cpp_compiler:"$CPP_COMPILER"
  

  rm -rf "$OUT_FOLDER"
  rm -f "$OUT_FOLDER.tar.gz"

  echo "$OUT_FOLDER";
  mkdir "$OUT_FOLDER";


  echo "--- SQLITE3 ---"
  "$C_COMPILER" $C_FLAGS -c -o "$OUT_FOLDER/libsqlite3.o" ./src/lib/sqlite3/sqlite3.c
  
  echo "--- CONFIG SDL2 ---"
  #There is an utility that guesses the SDL flags called sdl2-config
  #sdl2-config --libs --cflags will return
  #  -lSDL2
  #  -I/usr/include/SDL2 -D_REENTRANT
  SDL2_CONFIG_FLAGS=`sdl2-config --cflags --prefix=/usr/$PREFIX/sys-root/mingw/`   # -I/usr/include/SDL2 -D_REENTRANT
  SDL2_CONFIG_LIBS=`sdl2-config --libs  --prefix=/usr/$PREFIX/sys-root/mingw/`     # -lSDL2

  echo "  flags:" "$SDL2_CONFIG_FLAGS"
  echo "  libs:" "$SDL2_CONFIG_LIBS"


  echo "--- COMPILE_GFX $BITS ---"
  #must be linked BEFORE SDL libs, as it depends on SDL
  "$C_COMPILER" $C_FLAGS -c -o "$OUT_FOLDER/SDL2_rotozoom.o"       src/lib/SDL_gfx/SDL2_rotozoom.c
  "$C_COMPILER" $C_FLAGS -c -o "$OUT_FOLDER/SDL2_imageFilter.o"    src/lib/SDL_gfx/SDL2_imageFilter.c
  "$C_COMPILER" $C_FLAGS -c -o "$OUT_FOLDER/SDL2_gfxPrimitives.o"  src/lib/SDL_gfx/SDL2_gfxPrimitives.c
  "$C_COMPILER" $C_FLAGS -c -o "$OUT_FOLDER/SDL2_framerate.o"      src/lib/SDL_gfx/SDL2_framerate.c



  echo "--- COMPILE_WIN$BITS ---"
  CPP_FILES=$(<compile-cpp.txt)
  
  #SDL_MAIN_HANDLED must be defined to avoid link errors for  WinMain@16

  "$CPP_COMPILER" -o "$OUT_FOLDER/$OUT_BIN" $CPP_FLAGS \
  -DSDL_MAIN_HANDLED \
  -I src/lib \
  $SDL2_CONFIG_FLAGS \
  $CPP_FILES \
  "$OUT_FOLDER/libsqlite3.o" \
  "$OUT_FOLDER/SDL2_rotozoom.o" \
  "$OUT_FOLDER/SDL2_imageFilter.o" \
  "$OUT_FOLDER/SDL2_gfxPrimitives.o" \
  "$OUT_FOLDER/SDL2_framerate.o" \
  $SDL2_CONFIG_LIBS \
  -lSDL2_image \
  -lSDL2_ttf \
  ;

  #cleanup obj
  rm -f "$OUT_FOLDER/libsqlite3.o"
  rm -f "$OUT_FOLDER/SDL2_rotozoom.o"
  rm -f "$OUT_FOLDER/SDL2_imageFilter.o"
  rm -f "$OUT_FOLDER/SDL2_gfxPrimitives.o"
  rm -f "$OUT_FOLDER/SDL2_framerate.o"

  
  
  #copy files
  echo "--- COPY WIN$BITS ---"
  
  cp    "config.txt"       "$OUT_FOLDER"
  cp -r "src/ubuntu-mono"  "$OUT_FOLDER"
  cp -r "sql/"             "$OUT_FOLDER"
  
  #copy dll
  cp "/usr/$PREFIX/sys-root/mingw/bin/SDL2.dll"       "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/SDL2_image.dll" "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/SDL2_ttf.dll"   "$OUT_FOLDER/"
  
  cp "/usr/$PREFIX/sys-root/mingw/bin/libjpeg-62.dll"      "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libpng16-16.dll"     "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libtiff-5.dll"       "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libwebp-7.dll"       "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libfreetype-6.dll"   "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libharfbuzz-0.dll"   "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/zlib1.dll"           "$OUT_FOLDER/"  
  cp "/usr/$PREFIX/sys-root/mingw/bin/libssp-0.dll"        "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libbz2-1.dll"        "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libglib-2.0-0.dll"   "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libintl-8.dll"       "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libpcre-1.dll"       "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/iconv.dll"           "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libwinpthread-1.dll" "$OUT_FOLDER/"
  cp "/usr/$PREFIX/sys-root/mingw/bin/libstdc++-6.dll"     "$OUT_FOLDER/"
}

rm -f "image_annotate-win32.zip"
rm -f "image_annotate-win64.zip"


compile_windows 32 i686-w64-mingw32
cp "/usr/i686-w64-mingw32/sys-root/mingw/bin/libgcc_s_dw2-1.dll"  "$OUT_FOLDER/"
zip -rm9 "image_annotate-win32.zip" "image_annotate-win32"

echo "";
echo "==="
echo "";

compile_windows 64 x86_64-w64-mingw32
cp "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libgcc_s_seh-1.dll" "$OUT_FOLDER/"
zip -rm9 "image_annotate-win64.zip" "image_annotate-win64"

